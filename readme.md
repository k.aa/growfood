Для firefox (76.0.1 64x) на win10 нужен [geckodriver].

Списки тестов `python -m pytest --setup-show -v -l tests/`:  
- test_notify_city_work.py;
- test_next_item_slider.py;

Что доделать:
1) не работает перемещение по экрану к блоку;
1) все локаторы в отдельный файл;
1) убрать лишние переменные в ide, заменить на переменные(структуры) python;
1) написать yaml для CICD;
1) как в CICD запихать виртуалку;
1) проверить "браузер у нас в xvfb стартует -- и хром и огнелис";

_Create dep:_ `pip freeze > requirements.txt`  
_Install dep:_ `pip install -r requirements.txt`

[geckodriver]:https://github.com/mozilla/geckodriver/releases/tag/v0.26.0
[stackoverflow]: https://stackoverflow.com/questions/40208051/selenium-using-python-geckodriver-executable-needs-to-be-in-path
[webdriver_py]: https://selenium-python.readthedocs.io/index.html
[selenium]: https://www.selenium.dev/documentation/en/
[seleniumide]: https://www.selenium.dev/selenium-ide/docs/en/introduction/getting-started
[pytest]: https://docs.pytest.org/en/latest/
[se-pytest]: https://selenium-python.readthedocs.io/waits.html
[pattern_page_obj]: https://habr.com/ru/post/472156/
[decorator-1]: https://pythonworld.ru/osnovy/dekoratory.html
[decorator-2]: https://tproger.ru/translations/demystifying-decorators-in-python/
