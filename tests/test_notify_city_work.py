import pytest

from tests.pages.DefaultPage import DefaultPage
from tests.pages.SendingRequestPage import SendingRequest
from tests.conftest import driver
from tests.etc.consts import CityName


@pytest.mark.parametrize("city", CityName.LIST_CITIES)
def test_notify_city_work(driver, city):
    page = DefaultPage(driver)
    page.go_to_start_page()
    page.go_to_sending_request()

    target_page = SendingRequest(driver)
    target_page.enter_city(city)
    target_page.selected_city(city)
    assert target_page.should_be_notify_message()
