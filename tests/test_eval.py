import pytest


@pytest.mark.parametrize("test_input", ["3", "2", "6"])
def test_digit(test_input):
    assert test_input.isdigit()


@pytest.mark.parametrize("test_input,expected", [("3+5", 8), ("2+4", 6), ("6*9", 42)])
def test_eval(test_input, expected):
    assert eval(test_input) == expected
