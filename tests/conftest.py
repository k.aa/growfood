from selenium.webdriver import Firefox
import pytest


@pytest.fixture(scope="module")
def driver():
    driver = Firefox()
    yield driver
    driver.quit()
