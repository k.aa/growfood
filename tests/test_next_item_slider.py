from tests.pages.DefaultPage import DefaultPage
from tests.pages.EkaterinburgPage import PageEkaterinburg
from tests.conftest import driver


def test_next_item_slider(driver, item=2):
    page = DefaultPage(driver)
    page.go_to_start_page()
    page.go_to_ekaterinbutg()

    target_page = PageEkaterinburg(driver)
    assert target_page.should_be_active_prev_button(item) and target_page.should_be_active_pagination(item)
