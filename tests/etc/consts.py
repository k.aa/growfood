# CURRENT_CITY = 'current_city'
# NAVIGATION_MENU = 'nav_menu'
# FULL_NAVIGATION_MENU = 'full_nav_menu'

class CityName:
    CHANGE_CITY = 'Изменить город'
    KAZAN = 'Казань'
    EKAT = 'Екатеринбург'
    MOSCOW = 'Москва'
    SPB = 'Санкт-Петербург'
    NSB = 'Новосибирск'
    MURMANSK = 'Мурманск'
    KRASNODAR = 'Краснодар'

    LIST_CITIES = [MOSCOW, EKAT, NSB]

# SLIDER_LEFT_BUTTON = 'slider_left_button'
# SLIDER_PAGINATION = 'slider_pagination'
# BLOCK_REVIEW = 'review_ekaterinburg'
#
# INPUT_CITY = 'input_city'
# FIND_CITY_LIST = 'find_city_in_list'
# MES_INPUT_CITY = 'mes_input_city'
