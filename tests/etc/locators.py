from selenium.webdriver.common.by import By
from tests.etc.consts import CityName


class LocatorsPageDefault:
    CITIES = {
        CityName.CHANGE_CITY: (By.XPATH, '//div[@class=\'b-city-selector__other-city-button\']'),
        CityName.KAZAN: (By.XPATH, '//button[contains(text(), \'Казани\')]'),
        CityName.EKAT: (By.XPATH, '//button[contains(text(), \'Екатеринбурге\')]'),
        CityName.SPB: (By.XPATH, '//button[contains(text(), \'Санкт-Петербурге\')]'),
        CityName.NSB: (By.XPATH, '//button[contains(text(), \'Новосибирске\')]'),
        CityName.MOSCOW: (By.XPATH, '//button[contains(text(), \'Москве\')]'),
        CityName.MURMANSK: (By.XPATH, '//button[contains(text(), \'Мурманске\')]'),
        CityName.KRASNODAR: (By.XPATH, '//button[contains(text(), \'Краснодаре\')]')
    }

    CURRENT_CITY = (By.CSS_SELECTOR, ".b-city-selector__current")
    NAVIGATION_MENU = (By.CSS_SELECTOR, ".icon-right")
    FULL_NAVIGATION_MENU = (By.CSS_SELECTOR, ".b-button__ghost-grey")


class LocatorsPageEkaterinburg:
    BLOCK_REVIEW = (By.ID, "reviews-v2")
    SLIDER_PAGINATION = (By.XPATH, "//div[@class=\'b-reviews-pagination swiper-pagination-bullets "
        "swiper-pagination-bullets-dynamic\']/")
    SLIDER_LEFT_BUTTON = (By.XPATH, "//div[@id=\'reviews-v2\']/descendant::div[contains(@class, \'b-reviews-prev\')]")
    SLIDER_RIGHT_BUTTON = (By.ID, "Oval-3-Copy")


class LocatorsPageSendingRequest:
    INPUT_CITY = (By.ID, "city-input")
    FIND_CITY_LIST = (By.XPATH, f"//div[contains(@class, \'suggest-item\')][1]//")
    MES_INPUT_CITY = (By.CSS_SELECTOR, ".c-input-group__message")
