from tests.pages.BaseApp import BasePage
from tests.etc import locators


def move_item_slider(count_click):
    def move(func):
        def wrapper(self, index_item):
            [self.find_element(locators.LocatorsPageEkaterinburg.SLIDER_RIGHT_BUTTON).click()
                for _ in range(count_click)]

            result = func(self, index_item)

            [self.find_element(locators.LocatorsPageEkaterinburg.SLIDER_LEFT_BUTTON).click()
                for _ in range(count_click)]

            return result

        return wrapper

    return move


class PageEkaterinburg(BasePage):
    def click_next_item_slider(self, count_click):
        [self.find_element(locators.LocatorsPageEkaterinburg.SLIDER_RIGHT_BUTTON).click() for _ in range(count_click)]

    def click_prev_item_slider(self, count_click):
        [self.find_element(locators.LocatorsPageEkaterinburg.SLIDER_LEFT_BUTTON).click() for _ in range(count_click)]

    # @move_item_slider(1)
    def should_be_active_prev_button(self, index_item):
        self.click_next_item_slider(index_item - 1)

        result = False if \
            self.find_element(locators.LocatorsPageEkaterinburg.SLIDER_LEFT_BUTTON) \
                .get_attribute("aria-disabled") == 'true' \
            else True

        self.click_prev_item_slider(index_item - 1)
        return result

    # @move_item_slider(1)
    def should_be_active_pagination(self, index_item):
        self.click_next_item_slider(index_item - 1)

        locator, path = locators.LocatorsPageEkaterinburg.SLIDER_PAGINATION
        path += f'span[{index_item}]'
        name_attr = self.find_element((locator, path)).get_attribute("class")

        self.click_prev_item_slider(index_item - 1)
        return True if name_attr.find('active') else False
