from tests.pages.BaseApp import BasePage
from tests.etc import locators


class SendingRequest(BasePage):
    def enter_city(self, city):
        search_field = self.find_element(locators.LocatorsPageSendingRequest.INPUT_CITY)
        # search_field.click()
        search_field.send_keys(city)

    def selected_city(self, city):
        locator, path = locators.LocatorsPageSendingRequest.FIND_CITY_LIST
        path = path + f"div[contains(text(), \'{city}\')]"

        self.find_element((locator, path)).click()

    def should_be_notify_message(self):
        if self.is_element_present(locators.LocatorsPageSendingRequest.MES_INPUT_CITY):
            message = self.find_element(locators.LocatorsPageSendingRequest.MES_INPUT_CITY).text
            return True if message == 'Отличная новость! Тут мы уже работаем. Посмотреть' else False
        else:
            return False
