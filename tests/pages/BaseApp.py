from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class BasePage:

    def __init__(self, driver):
        self.driver = driver
        self.base_url = "https://growfood.pro/"
        self.time = 10

    def find_element(self, locator, time=10):
        return WebDriverWait(self.driver, time).until(EC.presence_of_element_located(locator),
            message=f"Can't find element by locator {locator}")

    def find_elements(self, locator, time=10):
        return WebDriverWait(self.driver, time).until(EC.presence_of_all_elements_located(locator),
            message=f"Can't find elements by locator {locator}")

    def is_element_present(self, locator):
        try:
            self.find_element(locator)
        except Exception:
            return False
        return True

    def go_to_start_page(self):
        return self.driver.get(self.base_url)
