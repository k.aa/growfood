from tests.pages.BaseApp import BasePage

from tests.etc import locators
from tests.etc.consts import CityName


class DefaultPage(BasePage):
    def go_to_city(self, target_city):
        cur_city = self.find_element(locators.LocatorsPageDefault.CURRENT_CITY).text

        if cur_city != target_city:
            self.find_element(locators.LocatorsPageDefault.NAVIGATION_MENU).click()
            self.find_element(locators.LocatorsPageDefault.FULL_NAVIGATION_MENU).click()
            self.find_element(locators.LocatorsPageDefault.CITIES[target_city]).click()

    def go_to_ekaterinbutg(self):
        self.go_to_city(CityName.EKAT)

    def go_to_sending_request(self):
        self.go_to_city(CityName.CHANGE_CITY)
